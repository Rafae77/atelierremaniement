package ca.qc.claurendeau;
import ca.qc.claurendeau.Client;
import ca.qc.claurendeau.Location;
import ca.qc.claurendeau.Outil;
import junit.framework.TestCase;

public class ClientTest extends TestCase {

	protected void setUp() throws Exception {
		super.setUp();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}

	public void testClient() {
		Client c = new Client("Roger");
		assertNotNull(c);	
	}

	public void testAjoutLocation() {
		Client client2 = new Client("Sallie");
		Outil outil1 = new Outil("Marteau piqueur", Outil.REGULIER);
		Location locat1 = new Location(outil1, 3); // 3 jours de location
		client2.ajoutLocation(locat1);
	}

	public void testGetName() {
		Client c = new Client("Roger");
		assertEquals("Roger", c.getNom());
	}

	public void testRapportOutilRegulier() {
		Client client2 = new Client("Sallie");
		Outil outil1 = new Outil("Marteau piqueur", Outil.REGULIER);
		Location locat1 = new Location(outil1, 3); // 3 jours de location
		client2.ajoutLocation(locat1);
		String expected = "Etat des locations effectuées de Sallie :\n" +
							"\tMarteau piqueur\t3.5\n" +
							"Montant du égale à : 3.5\n" +
							"Vous gagnez : 1 points de bonis";
		String rapport = client2.rapport();
		assertEquals(expected, rapport);
	}
	
	public void testRapportNouveaute() {
		Client client2 = new Client("Sallie");
		Outil outil1 = new Outil("Auto-marteau", Outil.NOUVEAUTEES);
		Location locat = new Location(outil1, 3); // 3 jours de location
		client2.ajoutLocation(locat);
		String expected = "Etat des locations effectuées de Sallie :\n" +
		"\tAuto-marteau\t9.0\n" +
		"Montant du égale à : 9.0\n" +
		"Vous gagnez : 2 points de bonis";
		String rapport = client2.rapport();
		assertEquals(expected, rapport);
	}
	
	public void testRapportGrosOutillage() {
		Client client2 = new Client("Sallie");
		Outil outil1 = new Outil("Compresseur hydraulique", Outil.GROS_OUTILLAGE);
		Location locat1 = new Location(outil1, 3); // 3 jours de location
		client2.ajoutLocation(locat1);
		String expected = "Etat des locations effectuées de Sallie :\n" +
		"\tCompresseur hydraulique\t1.5\n" +
		"Montant du égale à : 1.5\n" +
		"Vous gagnez : 1 points de bonis";
		String rapport = client2.rapport();
		assertEquals(expected, rapport);
	}
}
