package ca.qc.claurendeau;
/**
 * Permet de documenter les locations de matériel
 * @author 03738
 *
 */
public class Location {
	private Outil _outil;
	private int _nbJoursLoues;
	
	public Location(Outil outil, int nbJoursLoues) {
		_outil = outil;
		_nbJoursLoues = nbJoursLoues;
	}

	public int getNbJoursLoues() {
		return _nbJoursLoues;
	}

	public Outil getOutil() {
		return _outil;
	}
}
